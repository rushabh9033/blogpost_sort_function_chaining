# **Introduction to Sort**

The sort method allows you to sort the elements of an array in place. The sort method changes the position of elements in the original array instead of returning a new array.

The sort method casts elements to strings and compares the strings to determine the orders.

# **How to sort strings?**

Sort String in JavaScript is used to sort the strings according to alphabetical order i.e it converts the given elements into strings by comparing the UTF-16 code sequences into unit values. Sort String in JavaScript is used to sort the strings according to alphabetical order i.e it converts the given elements into strings by comparing the UTF-16 code sequences into unit values.

- We have two ways to sort strings as Using the sort() method or Using Loop.

- On using the sort() method, we are using a predefined method by JavaScript to sort an array of strings.

- Sort() method is applicable only for alphabetic strings.

```javascript
// Example: 1
let str = ["Raju", "Ramesh", "Suresh", "kamlesh"];
let new_str = str.sort();
console.log(new_str);
```

```
Output:

[Kamlesh, Raju, Ramesh, Suresh]
```

```javascript
// Example: 2 using comparision function
let str = [
  "nest",
  "Eggs",
  "bite",
  "gator",
  "caYman",
  "Grip",
  "grips",
  "Jaw",
  "crocodilian",
  "Bayou",
];

function sortStr(a, b) {
  a = a.toLowerCase();
  b = b.toLowerCase();

  if (a > b) {
    return 1;
  } else if (a < b) {
    return -1;
  } else if (a === b) {
    return 0;
  }
}
let newStr = str.sort(sortStr);
console.log(newStr);
```

```
Output:

[
  'Bayou',  'bite',
  'caYman', 'crocodilian',
  'Eggs',   'gator',
  'Grip',   'grips',
  'Jaw',    'nest'
]
```

In the above example, the sortStr function first converts the whole character into a lowercase letter and then compares this value.

# **How to sort numbers?**

There are sorting performed on Number in two types ascending sorting and descending sortings. There are different types of methods available for sorting.

```javascript
// Example: 1 Simple number sorting in ascending order
const num = [26, 25, 33, 32, 40, 21];
let newNum = num.sort();
console.log(newNum);
```

```
Output:

[ 21, 25, 26, 32, 33, 40 ]
```

Here, I used the inbuilt sort function which changed the positions of a given array.

```javascript
// Example: 2 Sorting in descending order
const numbers = [5, 13, 1, 44, 32, 15, 500];
let sortNumber = numbers.sort((a, b) => b - a);
console.log(sortNumber);
```

```
Output:

[100,44,32,15,13,5,1]
```

In the above example, we used the callback function which passes into to sort function which calls on every array element and compares them.

```javascript
// Example: 3
const numArray = [1400, 104, 99, 89, 23];
let newArr = numArray.sort(function (a, b) {
  return a - b;
});

console.log(newArr);
```

```
Output:

[ 23, 89, 99, 104, 1400 ]
```

In the below example I perform sorting on an array of an object which sorts the data according to its age and also callback function work on every object element.

```javascript
// Example: 4
let students = [
  { name: "Ramesh", age: 16 },
  { name: "Suresh", age: 14 },
  { name: "Arjun", age: 14 },
  { name: "Rajesh", age: 15 },
];

let sorted_students = students.sort(function (a, b) {
  return a.age - b.age;
});

console.log(sorted_students);
```

```
Output:

[
  { name: 'Suresh', age: 14 },
  { name: 'Arjun', age: 14 },
  { name: 'Rajesh', age: 15 },
  { name: 'Ramesh', age: 16 }
]
```

# **How to sort dates?**

The easiest way to sort an array by date in JavaScript is to use the Array.prototype.sort method along with subtracting one JavaScript date from another. Here, I would describe some examples of how to sort the array or object by date.

```javascript
// Example: 1
const arrayOfDates = [
  { date: new Date("12/25/2021") },
  { date: new Date("12/24/2021") },
  { date: new Date("12/26/2021") },
];

const sortedDates = arrayOfDates.sort(
  (dateA, dateB) => dateA.date - dateB.date
);

consol.log(sortedDates);
```

```
Output:

[{date: "12/24/2020" }, { date: "12/25/2020" }, { date: "12/26/2020" }]
```

The sort method accepts a callback where you need to return a positive or negative number that it can then use to determine the order of the array.

```javascript
const array = [
  {
    id: 1,
    name: "test1",
    date: "2020-01-05",
  },
  {
    id: 2,
    name: "test2",
    date: "2020-01-02",
  },
];

const newArr = array.sort(function (a, b) {
  let dateA = new Date(a.date);
  let dateB = new Date(b.date);
  return dateA - dateB;
});

console.log(newArr);
```

```
Output:

[
  { id: 2, name: 'test2', date: '2020-01-02' },
  { id: 1, name: 'test1', date: '2020-01-05' }
]
```

We can reverse the sort order simply by inverting the return value of the function.

```javascript
const arrayOfDates = ["2021-01-12", "2021-02-10", "2021-05-15", "2021-03-20"];

const newArr = arrayOfDates.sort(function (a, b) {
  let dateA = new Date(a);
  let dateB = new Date(b);
  return dateB - dateA;
});

console.log(newArr);
```

```
Output:

[ '2021-05-15', '2021-03-20', '2021-02-10', '2021-01-12' ]
```
